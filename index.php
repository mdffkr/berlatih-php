<?php

    require('animal.php');

    $object = new animal("kuda");

    echo "Nama Hewan : ".$object->name."<br>";
    echo "Jumlah kaki : ".$object->legs."<br>";
    echo "Cold Blooded : ".$object->cold_blooded."<br>";

    $object2 = new frog("frog");

    echo "Nama Hewan : ".$object2->name."<br>";
    echo "Jumlah kaki : ".$object2->legs."<br>";
    echo "Cold Blooded : ".$object2->cold_blooded."<br>";

    $object3 = new ape("ape");

    echo "Nama Hewan : ".$object3->name."<br>";
    echo "Jumlah kaki : ".$object3->legs."<br>";
    echo "Cold Blooded : ".$object3->cold_blooded."<br>";